package main;

public class Main {

	public static void main(String[] args) {
		
		int n1 = 5;
		int n2 = 2;
		
		maximo(n1,n2);
		minimo(n1,n2);
		potencia(n1,n2);

	}

	private static void potencia(int n1, int n2) {
		
		System.out.println(Math.pow(n1,n2));
		
	}

	private static void minimo(int n1, int n2) {
		
		System.out.println(Math.min(n1,n2));
		
	}

	private static void maximo(int n1, int n2) {
		
		System.out.println(Math.max(n1,n2));
		
	}
}
